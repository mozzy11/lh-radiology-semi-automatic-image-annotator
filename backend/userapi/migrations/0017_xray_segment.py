# Generated by Django 2.1.5 on 2020-08-02 09:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('userapi', '0016_auto_20200719_0249'),
    ]

    operations = [
        migrations.AddField(
            model_name='xray',
            name='segment',
            field=models.ImageField(blank=True, null=True, upload_to='media/xray'),
        ),
    ]
