"""This is the module reponsible for defining SQL Models"""
from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.contrib.auth.models import User

class UserProfile(models.Model):
    """
    This class will represent user profile
    """
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    profile_picture = models.ImageField(upload_to='media/profile_pictures', blank=True)
    name = models.CharField(default='', max_length=50)

    def __str__(self):
        return self.name

def save_UserProfile(sender, instance, **kwargs):
    if not (UserProfile.objects.filter(user=instance)):
        UserProfile.objects.create(user=instance, name=instance.username)

post_save.connect(save_UserProfile, sender=User)

class Xray(models.Model):
    """
    This class represents xray model
    """
    user_profile = models.ForeignKey(UserProfile, null=True, blank=True, on_delete=models.CASCADE)
    picture = models.ImageField(upload_to='media/xray', null=True, blank=True)
    segment = models.ImageField(upload_to='media/xray', null=True, blank=True)
    pic_name = models.CharField(max_length=50, unique=True)
    expert_check = models.BooleanField(default=0)
    height = models.IntegerField(default=-1)
    width = models.IntegerField(default=-1)

    def __str__(self):
        return self.pic_name

class DLModel(models.Model):
    """
    This class represents DLModel
    """
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name

class Disease(models.Model):
    """
    This class represents Disease
    """
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name

class Log(models.Model):
    """
    This class represents log
    """
    username = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    disease  = models.ForeignKey(Disease, on_delete=models.CASCADE)

    def __str__(self):
        return self.username.name

class Bbox(models.Model):
    """
    This class represents bounding box model
    """
    xray = models.ForeignKey(Xray, related_name="bboxes", on_delete=models.CASCADE)
    xmin = models.FloatField(default=0)
    ymin = models.FloatField(default=0)
    xmax = models.FloatField(default=100)
    ymax = models.FloatField(default=100)
    disease = models.ForeignKey(Disease, default=1, on_delete=models.CASCADE)
    dl_model = models.ForeignKey(DLModel, default=1, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.xray.pic_name

class Segment(models.Model):
    """
    This class represents segmentation points
    """
    xray = models.ForeignKey(Xray, related_name="segments", on_delete=models.CASCADE)
    points = models.CharField(default="", max_length=10000, unique=False)
    
    def __str__(self):
        return self.xray.pic_name
