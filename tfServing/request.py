"""This module is responsible for handling requests to the TF-Serving and Django Backend"""
import os
import json
import asyncio
from io import BytesIO
import base64
import cv2
import numpy as np
import requests_async as requests
from PIL import Image
import time
from utils import *

# Wait for 5 seconds
time.sleep(10)

def parse_yolo(json_response, img_names, uint_imgs, model_name):
    """
    This function is responsible for parsing and sending the response from Yolo
    to Django backend

    Args:
        json_response (json): json recieved from TF-Serving
        img_names (list): list of image names
        uint_imgs (list): list of images in numpy array form with data in uint format
        model_name (str): string of model name
    """
    preds = json.loads(json_response.text)
    preds = preds['predictions']
    final_list = []
    for i, pred in enumerate(preds):
        scores = pred['yolo_nms_1']
        classes = pred['yolo_nms_2']
        bboxes = pred['yolo_nms']
        bbox_dict = get_bboxes(bboxes, classes, model_name)
        bbox_dict['pic_name'] = img_names[i][0]
        pil_img = Image.fromarray(uint_imgs[i])
        buff = BytesIO()
        pil_img.save(buff, format="JPEG")
        new_image_string = base64.b64encode(buff.getvalue()).decode("utf-8")
        bbox_dict['picture'] = new_image_string
        bbox_dict['height'] = 1024
        bbox_dict['width'] = 1024
        final_list.append(bbox_dict)
    return final_list

def parse_od_output(pred, model_name):
    """
    This function is responsible for parsing the object_detection out for Retinanet model

    Args:
        pred (dict): dictionary of predictions from the Retinanet Model
        model_name (str): string of model_name
    """
    indx_list = []
    for indx, score in enumerate(pred['detection_scores']):
        if(score > .2):
            indx_list.append(indx)
    det_boxes = []
    for indx in indx_list:
        det_boxes.append(pred["detection_boxes"][indx])
    det_classes = []
    for indx in indx_list:
        det_classes.append(pred["detection_classes"][indx])
    bbox_dict = get_bboxes(det_boxes, det_classes, model_name)
    return bbox_dict

def parse_retina(json_response, img_names, uint_imgs, model_name):
    """
    This function is responsible for parsing and sending the response from RetinaNet
    to Django backend

    Args:
        json_response (json): json recieved from TF-Serving
        img_names (list): list of image names
        uint_imgs (list): list of images in numpy array form with data in uint format
        model_name (str): string of model name
    """
    preds = json.loads(json_response.text)
    preds = preds['predictions']
    final_list = []
    for i, p in enumerate(preds):
        img_name = img_names[i][0]
        bbox_dict = parse_od_output(p, "retinanet")
        bbox_dict["pic_name"] = img_name
        final_list.append(bbox_dict)
    return final_list

def parse_unet(json_response, img_names, uint_imgs):
    """
    This function is responsible for parsing and sending the response from Unet
    to Django backend

    Args:
        json_response (json): json recieved from TF-Serving
        img_names (list): list of image names
        uint_imgs (list): list of images in numpy array form with data in uint format
    """
    preds = json.loads(json_response.text)
    preds = preds['predictions']
    final_list = []
    for i, p in enumerate(preds):
        arr = np.array(p)
        arr = (arr > 0.00000005).astype('uint8')
        arr = np.reshape(arr, (256, 256))
        arr = cv2.resize(arr, (1024, 1024), interpolation = cv2.INTER_CUBIC)
        kernel = np.ones((5,5), np.uint8) 
        arr = cv2.erode(arr, kernel, iterations=1) 
        arr = cv2.dilate(arr, kernel, iterations=1)
        arr = np.reshape(arr, (1024, 1024, 1))
        arr = np.multiply(arr, [1.,0.,0.])
        arr = np.multiply(uint_imgs[i],arr)
        arr = arr.astype(np.uint8)
        arr = cv2.addWeighted(arr, 0.6, uint_imgs[i], 0.4, 0)
        arr = arr.astype(np.uint8)
        img = Image.fromarray(arr)
        buff = BytesIO()
        img.save(buff, format="JPEG")
        new_image_string = base64.b64encode(buff.getvalue()).decode("utf-8")
        final_list.append(new_image_string)
    return final_list
      
def combine_lists(model_list):
    """
    This function is responsible for adding bounding box information from multiple models

    Args:
        model_list (list): list of outputs from different models
    """
    l = len(model_list)
    x = []
    md = {}
    main_model = model_list[0]
    for j, instances in enumerate(main_model):
        for i in range(1, l):
            next_model = model_list[i]
            bbs = next_model[j]
            if(next_model[j]["pic_name"]==main_model[j]["pic_name"]):
                for bb in bbs["bboxes"]:
                    main_model[j]["bboxes"].append(bb)
    
    return main_model

async def main():
    """
    This function is responsible for parsing and sending dataset to the TF-Serving
    server for inference
    """
    path = os.path.abspath('./dataset')
    files = os.listdir(path)
    counter = 0
    imgs = []
    uint_imgs = []
    img_names = []
    uint_img_256 = []
    for img in files:
        counter = counter + 1
        img_path = os.path.join(path, img)
        img_names.append((img, img_path))
        img = cv2.imread(img_path)
        uint_img = cv2.imread(img_path)
        img_256 = cv2.resize(uint_img, (256, 256), interpolation = cv2.INTER_CUBIC) 
        img = cv2.resize(uint_img, (512, 512), interpolation = cv2.INTER_CUBIC)
        imgs.append(img)
        uint_imgs.append(uint_img)
        uint_img_256.append(img_256)
        if counter%1 == 0:
            arr = np.array(uint_imgs)/255
            arr1 = np.array(imgs)
            arr2 = np.array(uint_img_256)
            data = json.dumps({"signature_name":"serving_default", "instances":arr.tolist()})
            data1 = json.dumps({"signature_name":"serving_default", "instances":arr1.tolist()})
            data_256 = json.dumps({"signature_name":"serving_default", "instances":arr2.tolist()})
            headers = {"content-type":"application/json"}
            tf_serving_url = 'http://localhost:8501'
            """
            Add your new models here
            """

            json_response = await requests.post(f'{tf_serving_url}/v1/models/yolov3:predict',
                                                data=data,
                                                headers=headers)
            yolo_list = parse_yolo(json_response, img_names, uint_imgs, "YOLO V3")
            
            json_response = await requests.post(f'{tf_serving_url}/v1/models/retinanet:predict',
                                                data=data1,
                                                headers=headers)
            retina_list = parse_retina(json_response, img_names, uint_imgs, "retinanet")

            json_response = await requests.post(f'{tf_serving_url}/v1/models/unet:predict',
                                                data=data_256,
                                                headers=headers)
            unet = parse_unet(json_response, img_names, uint_imgs)
            
            tmp_list = [yolo_list, retina_list]
            final_list = combine_lists(tmp_list)
            for _i, seg in enumerate(unet):
                yolo_list[_i]["segment"] = seg
            await add_xray(final_list)
            
            imgs = []
            uint_imgs = []
            img_names = []
            uint_img_256 = []

if __name__ == "__main__":
    asyncio.run(main())
