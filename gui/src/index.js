import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import CssBaseline from '@material-ui/core/CssBaseline';
import {MuiThemeProvider, createMuiTheme } from '@material-ui/core';

const theme = createMuiTheme({
  palette:{
    type: 'dark',
    primary: {
      light:"#546e7a",
      main:"#37474f",
      dark:"#263238",
      contrastText:"#fafafa",
    },

    secondary: {
      light:"#fb8c00",
      main:"#f59031",
      dark:"#e65100",
      contrastText:"#fafafa",
    },

    background: {
      paper: "#37474f",
      default: "#263238",
    }
    
  },


});


ReactDOM.render(
  <React.StrictMode>
    <MuiThemeProvider theme={theme}>
      <CssBaseline/>
        <App />        
    </MuiThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
